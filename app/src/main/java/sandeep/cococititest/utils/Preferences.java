package sandeep.cococititest.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by sandeepgurram on 9/3/2015.
 */
public class Preferences {
    private static SharedPreferences.Editor mEditor;
    private static SharedPreferences mPrefs;
    private final Context mContext;


    public Preferences(Context context) {
        //intialising preferences
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        mEditor = mPrefs.edit();
        this.mContext = context;
    }

    public void add(String name, int value) {
        mEditor.putInt(name, value);
        mEditor.commit();
    }

    public void add(String name, float value) {
        mEditor.putFloat(name, value);
        mEditor.commit();
    }

    public void add(String name, long value) {
        mEditor.putLong(name, value);
        mEditor.commit();
    }

    public void add(String name, String value) {
        mEditor.putString(name, value);
        mEditor.commit();
    }

    public void add(String name, boolean value) {
        mEditor.putBoolean(name, value);
        mEditor.commit();
    }

    public int getInt(String name) {
        return mPrefs.getInt(name, 0);
    }

    public float getFloat(String name) {
        return mPrefs.getFloat(name, 0.0f);
    }

    public long getLong(String name) {
        return mPrefs.getLong(name, 0);
    }

    public double getDouble(String name) {
        return mPrefs.getFloat(name, 0f);
    }

    public String getString(String name) {
        return mPrefs.getString(name, "");
    }

    /**
     * Stores the boolean value with false as defaulf value.
     *
     * @param name
     * @return value stored img_in preferences
     */
    public boolean getBoolean(String name) {
        return mPrefs.getBoolean(name.toString(), false);
    }

    /**
     * Stores the boolean value with default value.
     *
     * @param name
     * @param defaultValue Default value to store.
     * @return value stored img_in preferences
     */
    public boolean getBoolean(String name, boolean defaultValue) {
        return mPrefs.getBoolean(name.toString(), defaultValue);
    }


    public boolean hasKey(String name) {
        return (PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext()).contains(name));
    }

    public void clearPrefs() {
        mPrefs.edit().clear().commit();
    }

    public SharedPreferences getPrefs() {
        return mPrefs;
    }

    public SharedPreferences.Editor getEditor() {
        return mEditor;
    }

    public ArrayList<String> getArrayList(String prefsName) {

//        if(hasKey(prefsName)){
//            return null;
//        }

        ArrayList<String> arrayList_StoredItems = new ArrayList<>();
        String jsonString = getString(prefsName);


        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            for (int index = 0; index < jsonArray.length(); index++) {
                try {
                    arrayList_StoredItems.add(jsonArray.get(index).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }

        return arrayList_StoredItems;
    }

    /**
     * Stores arraylist to Prefs
     *
     * @param arrayList arraylist to store
     */
    public void add(String prefsName, ArrayList<String> arrayList) {
        String[] convertedArray = new String[arrayList.size()];
        for (int index = 0; index < convertedArray.length; index++)
            convertedArray[index] = arrayList.get(index);
        JSONArray jsonArray = new JSONArray(Arrays.asList(convertedArray));
        add(prefsName, jsonArray.toString());
    }

    public String getString(String prefsName, String defaultValue) {
        return mPrefs.getString(prefsName, defaultValue);
    }

    public boolean hasInt(String string) {
        return mPrefs.contains(string);
    }

    public boolean hasString(String string) {
        return mPrefs.contains(string);
    }

    public void add(String strKey, JSONObject jsonObject) {
        mEditor.putString(strKey, jsonObject.toString());
        mEditor.commit();
    }

    public JSONObject getJSON(String name) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(mPrefs.getString(name, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public boolean hasJSON(String key) {
        return (PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext()).contains(key));
    }

    public void add(String prefName, Serializable serializable) {
        String data = new Gson().toJson(serializable);
        add(prefName, data);
    }

    public <E> E getSerializable(String prefName, Class<E> eClass) {
        E object2 = (E) new Gson().fromJson(getString(prefName), eClass);
        return object2;
    }
}

