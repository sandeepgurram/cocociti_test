package sandeep.cococititest.model.requestModel;

import com.google.gson.annotations.SerializedName;

public class RequestLogin {

    @SerializedName("user")
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return
                "RequestLogin{" +
                        "user = '" + user + '\'' +
                        "}";
    }

    public static class User {

        @SerializedName("password")
        private String password;

        @SerializedName("email")
        private String email;

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPassword() {
            return password;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        @Override
        public String toString() {
            return
                    "User{" +
                            "password = '" + password + '\'' +
                            ",email = '" + email + '\'' +
                            "}";
        }
    }
}