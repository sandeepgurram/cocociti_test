package sandeep.cococititest.model.responseModel;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin {

    @SerializedName("data")
    private Data data;

    @SerializedName("status")
    private String status;

    @SerializedName("info")
    private String info;

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    @Override
    public String toString() {
        return
                "ResponseLogin{" +
                        "data = '" + data + '\'' +
                        ",status = '" + status + '\'' +
                        ",info = '" + info + '\'' +
                        "}";
    }

    public static class User {

        @SerializedName("access_token")
        private String accessToken;

        @SerializedName("email")
        private String email;

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        @Override
        public String toString() {
            return
                    "User{" +
                            "access_token = '" + accessToken + '\'' +
                            ",email = '" + email + '\'' +
                            "}";
        }
    }

    public static class Data {

        @SerializedName("user")
        private User user;

        public void setUser(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        @Override
        public String toString() {
            return
                    "Data{" +
                            "user = '" + user + '\'' +
                            "}";
        }
    }
}