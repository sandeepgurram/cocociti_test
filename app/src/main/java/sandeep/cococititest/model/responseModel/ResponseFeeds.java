package sandeep.cococititest.model.responseModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseFeeds {

    @SerializedName("showcases")
    private List<ShowcasesItem> showcases;

    @SerializedName("status")
    private String status;

    @SerializedName("info")
    private String info;

    public void setShowcases(List<ShowcasesItem> showcases) {
        this.showcases = showcases;
    }

    public List<ShowcasesItem> getShowcases() {
        return showcases;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    @Override
    public String toString() {
        return
                "ResponseFeeds{" +
                        "showcases = '" + showcases + '\'' +
                        ",status = '" + status + '\'' +
                        ",info = '" + info + '\'' +
                        "}";
    }

    public static class ShowcasesItem {

        @SerializedName("year")
        private String year;

        @SerializedName("user_id")
        private long userId;

        @SerializedName("description")
        private String description;

        @SerializedName("id")
        private int id;

        @SerializedName("title")
        private String title;

        public void setYear(String year) {
            this.year = year;
        }

        public String getYear() {
            return year;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public long getUserId() {
            return userId;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        @Override
        public String toString() {
            return
                    "ShowcasesItem{" +
                            "year = '" + year + '\'' +
                            ",user_id = '" + userId + '\'' +
                            ",description = '" + description + '\'' +
                            ",id = '" + id + '\'' +
                            ",title = '" + title + '\'' +
                            "}";
        }
    }
}