package sandeep.cococititest.network;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import sandeep.cococititest.model.requestModel.RequestLogin;
import sandeep.cococititest.model.responseModel.ResponseFeeds;
import sandeep.cococititest.model.responseModel.ResponseLogin;

/**
 * Created by Sandeep Gurram on 6/9/2017.
 */
public interface ApiInterface {

    @Headers({
            "Content-Type:application/json",
            "Accept:application/json"
    })
    @POST("users/sign_in")
    Call<ResponseLogin> login(@Body RequestLogin apiKey);


    @Headers({
            "Content-Type:application/json",
            "Accept:application/json"
    })
    @GET("get_feeds")
    Call<ResponseFeeds> getFeed(@Header("X-ACCESS-TOKEN") String accessToken,
                                @Header("X-USER-EMAIL") String email);


}
