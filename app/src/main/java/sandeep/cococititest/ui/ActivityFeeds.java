package sandeep.cococititest.ui;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sandeep.cococititest.R;
import sandeep.cococititest.databinding.ActivityFeedsBinding;
import sandeep.cococititest.model.responseModel.ResponseFeeds;
import sandeep.cococititest.model.responseModel.ResponseLogin;
import sandeep.cococititest.network.ApiClient;
import sandeep.cococititest.network.ApiInterface;
import sandeep.cococititest.utils.Preferences;

public class ActivityFeeds extends AppCompatActivity {

    private static final String TAG = "ActivityFeeds";

    private ActivityFeedsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_feeds);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mBinding.content.recyclerview.setLayoutManager(new LinearLayoutManager(this));
        getFeed();


    }

    private void getFeed() {

        showProgressbar();

        String userStr = new Preferences(this).getString("user");
        Log.d(TAG, "getFeed: user = " + userStr);
        ResponseLogin user = new Gson().fromJson(userStr, ResponseLogin.class);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseFeeds> call = apiService.getFeed(user.getData().getUser().getAccessToken(),
                user.getData().getUser().getEmail());

        Log.d(TAG, "getFeed: URL = " + call.request().url().toString());

        call.enqueue(new Callback<ResponseFeeds>() {
            @Override
            public void onResponse(Call<ResponseFeeds> call, Response<ResponseFeeds> response) {
                Log.d(TAG, "Response: " + new Gson().toJson(response.body()));

                hideProgressBar();

                ResponseFeeds responseFeeds = response.body();

                if (responseFeeds.getStatus().equalsIgnoreCase("succes"))
                    updateUi(response.body().getShowcases());
                else {
                    Toast.makeText(ActivityFeeds.this, "Error getting list", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ResponseFeeds> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                hideProgressBar();
                Toast.makeText(ActivityFeeds.this, "Unable to login", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updateUi(List<ResponseFeeds.ShowcasesItem> showcases) {

        if (showcases != null && showcases.size() > 0) {
            AdapterFeeds adapter = new AdapterFeeds(this, showcases);
            mBinding.content.recyclerview.setAdapter(adapter);
        } else
            Toast.makeText(this, "No feed to show", Toast.LENGTH_LONG).show();

    }

    ProgressDialog mProgress;

    private void showProgressbar() {
        if (mProgress == null)
            mProgress = new ProgressDialog(this);
        else {
            if (mProgress.isShowing())
                mProgress.dismiss();
        }
        mProgress.setMessage("Fetching feeds");
        mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgress.setIndeterminate(true);
        mProgress.show();
    }

    private void hideProgressBar() {
        if (mProgress != null)
            mProgress.dismiss();
    }

}
