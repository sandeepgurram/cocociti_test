package sandeep.cococititest.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import sandeep.cococititest.databinding.ListitemFeedsBinding;
import sandeep.cococititest.model.responseModel.ResponseFeeds;

public class AdapterFeeds extends RecyclerView.Adapter<AdapterFeeds.ViewHolder> {

    private final String TAG = "AdapterFeeds";
    private final List<ResponseFeeds.ShowcasesItem> mShowcases;
    private Context mContext;

    public AdapterFeeds(Context context, List<ResponseFeeds.ShowcasesItem> showcases) {
        mContext = context;
        mShowcases = showcases;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListitemFeedsBinding binding = ListitemFeedsBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.binding.setItem(mShowcases.get(position));
        holder.binding.executePendingBindings();

    }


    @Override
    public int getItemCount() {
        return mShowcases.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ListitemFeedsBinding binding;

        public ViewHolder(ListitemFeedsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
