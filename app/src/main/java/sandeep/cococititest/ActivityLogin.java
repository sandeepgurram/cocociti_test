package sandeep.cococititest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sandeep.cococititest.databinding.ActivityLoginBinding;
import sandeep.cococititest.model.requestModel.RequestLogin;
import sandeep.cococititest.model.responseModel.ResponseLogin;
import sandeep.cococititest.network.ApiClient;
import sandeep.cococititest.network.ApiInterface;
import sandeep.cococititest.ui.ActivityFeeds;
import sandeep.cococititest.utils.Preferences;

public class ActivityLogin extends AppCompatActivity {

    private static final String TAG = "ActivityLogin";
    ActivityLoginBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void initLogin(View view) {

        if (validate())
            login();

    }

    private boolean validate() {
        boolean valid = true;

        valid = !TextUtils.isEmpty(mBinding.content.etUsername.getText()) && valid;
        if (TextUtils.isEmpty(mBinding.content.etUsername.getText())) {
            enableError(mBinding.content.tilUsername, mBinding.content.etUsername, "Enter email");
        }

        valid = !TextUtils.isEmpty(mBinding.content.etPassword.getText()) && valid;
        if (TextUtils.isEmpty(mBinding.content.etPassword.getText())) {
            enableError(mBinding.content.tilPassword, mBinding.content.etPassword, "Enter password");
        }

        return valid;
    }

    private void enableError(final TextInputLayout tilUsername, EditText etUsername, String errorMsg) {

        tilUsername.setError(errorMsg);
        tilUsername.setErrorEnabled(true);
        etUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tilUsername.setErrorEnabled(false);
            }
        });

    }

    private void login() {
        showProgressbar();
        final RequestLogin requestLogin = new RequestLogin();
        RequestLogin.User user = new RequestLogin.User();
        user.setEmail(mBinding.content.etUsername.getText().toString().trim());
        user.setPassword(mBinding.content.etPassword.getText().toString());
        requestLogin.setUser(user);


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseLogin> call = apiService.login(requestLogin);
        Log.d(TAG, "login: URL = " + call.request().url().toString());
        call.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                Log.d(TAG, "Response: " + response.toString());

                hideProgressBar();

                ResponseLogin responseLogin = response.body();

                if (responseLogin.getStatus().equalsIgnoreCase("success")) {
                    Log.d(TAG, "onResponse: obj = " + new Gson().toJson(responseLogin));
                    Toast.makeText(ActivityLogin.this, "Logged successfully", Toast.LENGTH_SHORT).show();
                    new Preferences(ActivityLogin.this).add("user", new Gson().toJson(responseLogin));

                    Intent intent = new Intent(getApplicationContext(), ActivityFeeds.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } else {
                    Toast.makeText(ActivityLogin.this, responseLogin.getInfo(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                hideProgressBar();
                Toast.makeText(ActivityLogin.this, "Unable to login", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ProgressDialog mProgress;

    private void showProgressbar() {
        if (mProgress == null)
            mProgress = new ProgressDialog(this);
        else {
            if (mProgress.isShowing())
                mProgress.dismiss();
        }
        mProgress.setMessage("Verifying credentials");
        mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgress.setIndeterminate(true);
        mProgress.show();
    }

    private void hideProgressBar() {
        if (mProgress != null)
            mProgress.dismiss();
    }
}


